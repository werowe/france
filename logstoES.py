import locale
import logging
import json
import hashlib
import random
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
import sys
import uuid
import os

def clean(row):
    d = row.asDict()
    d2 = {}
    for k, v in d.items():
        d2[k]=formatStr(v)
    return d2  
 
def formatStr(v):
   locale.setlocale(locale.LC_ALL, 'fr_FR')
   z=v.encode('ascii', 'ignore')
   zstr=z.decode("ascii")
   p=zstr.strip("%")
   try:
      return locale.atof(p)
   except ValueError:
      return v
      
def transform(data):
    locale.setlocale(locale.LC_ALL, 'fr_FR')
    j=json.dumps(data).encode('ascii', 'ignore')
    data['doc_id'] = hashlib.sha1(j).hexdigest()
    return (data['doc_id'], json.dumps(data,ensure_ascii=False))
    
    
def main():
    logger = logging.getLogger('py4j') 
    csvFile = sys.argv[1]
    y=csvFile.split("/")
    index = y[-1]
    
    conf = SparkConf().setAppName("load_csv_to_es")
    sc = SparkContext(conf=conf)
    sc.setLogLevel("INFO")
    spark = SQLContext(sc)


    df = spark.read.format("com.databricks.spark.csv").option("header", "true").option("encoding", "UTF-8").load(csvFile)

    rdd = df.rdd.map(clean)
    logger.info("rdd=", rdd.take(2))

    rdd2 = rdd.map(transform)
    logger.info("rdd2=", rdd2.take(2))

    esconf={}
    esconf["es.nodes"] = "localhost"
    esconf["es.port"] = "9200"
    esconf["es.resource"] = index + "/" + "logs" 
    esconf["es.input.json"] = "yes"
    esconf["es.mapping.id"] = "doc_id"
 
    rdd2.saveAsNewAPIHadoopFile(
        path='-',
        outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
        keyClass="org.apache.hadoop.io.NullWritable",
        valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
        conf=esconf)
  
      
if __name__ == "__main__":
    main()