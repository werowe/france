#!/bin/bash

[ $# -eq 0 ] && { echo "Usage: $0 <csv_name>"; exit 1; }

csvFile=$1
BASE_PATH="$HOME/crawler"


if [ -e $BASE_PATH/csv_data_sample/$csvFile ]
then
    echo "processing file $BASE_PATH/csv_data_sample/$csvFile"
else
    echo "file does not exist $BASE_PATH/csv_data_sample/$csvFile does not exist"
    exit 1
fi
 
NUM_CORES=*
DRIVER_MEM=1g

BASE_PATH="$HOME/crawler"

cd $BASE_PATH

export SPARK_HOME="/home/ubuntu/spark-2.3.0-bin-hadoop2.7"
export PATH=$SPARK_HOME/bin:$PATH
export SPARK_HOME="/home/ubuntu/spark-2.3.0-bin-hadoop2.7"
export PYSPARK_PYTHON=/usr/bin/python3
export PYTHONIOENCODING=utf8


$SPARK_HOME/bin/spark-submit --master local[$NUM_CORES] --driver-memory $DRIVER_MEM --jars $BASE_PATH/elasticsearch-hadoop-6.4.1/dist/elasticsearch-hadoop-6.4.1.jar $BASE_PATH/code/logstoES.py $BASE_PATH/csv_data_sample/$csvFile

