# -*- coding: utf-8 -*-

from pyspark            import SparkContext, SparkConf
from pprint             import pprint
import sys
from scrapy.utils.url   import canonicalize_url
from pyspark.sql import SQLContext
from pyspark.sql import functions as F
import hashlib
from url_process import cleaner


ES_WRITE_INDEX= sys.argv[2] + "/items"
CSV_FIELD_PREFIX= "csv_"
CONCAT_COLUMN_ONE="domain"
CONCAT_COLUMN_TWO="url"


# AS PER Elasticsearch-Scrapy key generation mechanism
def generate_record_id(url ):
    url = url.encode('utf-8')
    return hashlib.sha1(url).hexdigest()


def map_create_kv_pair(row):
#    canon_url = cleaner(row["url"])
# EDIT BY QUENTIN 19 OF MAY
    canon_url = cleaner(row["abs_url"])
    key = generate_record_id(canon_url)
    row_dict = row.asDict()
    value = {"_id" : key}
    for i in row_dict.keys():
        if row_dict[i] in (None,"-"):
            continue
        value[CSV_FIELD_PREFIX + i] = row_dict[i]
    #value["url"] = canon_url
    return (key,value)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "CSV FILE or Elasticsearch index name Argument Missing"
    csv_file_path = sys.argv[1]
    conf = SparkConf().setAppName("ES_CSV_MERGE")
    sc = SparkContext(conf=conf)
    sc.setLogLevel("INFO")
    sqlContext = SQLContext(sc)

    csv = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("delimiter",";").load(sys.argv[1])
### ADDED BU QUENTIN 18 06 2018 ###
### IF CSV CONTAINS FULL URL (https://www.mywebsite.com/dir1/dir2/page.html) ####
    csv = csv.withColumn('abs_url',F.col(CONCAT_COLUMN_TWO))

### IF CSV CONTAINS PARTIAL URL (/dir1/dir2/page.html) THEN CONCAT DOMAIN AND URL ####
#    csv = csv.withColumn('abs_url',F.concat(F.col(CONCAT_COLUMN_ONE), F.col(CONCAT_COLUMN_TWO)))

### THIS I JUST FO TEST PURPOSE - SHOULD BE CLEANED ASAP:
#    csv_column_one = csv.F.col(CONCAT_COLUMN_ONE)
#    csv_column_two = csv.F.col(CONCAT_COLUMN_TWO)
#    if "http" not in csv_column_one:

    csv = csv.groupBy("abs_url").pivot("bot").agg(F.count("bot").alias("count")).na.fill(0).orderBy('abs_url')
    csv = csv.rdd.map(map_create_kv_pair)
    #csv = csv.collect()
    #pprint(csv[:2])
    #dd = csv.take(5)
    #pprint(dd)
    #exit()

    print "-------------------- SAVING TO ELK STACK ---------------------------"
    es_write_conf = {
        "es.nodes" : "localhost",
        "es.port" : "9200",
        "es.resource" : ES_WRITE_INDEX,
        "es.write.operation": "upsert",
        "es.mapping.id": "_id",
	"es.mapping.exclude": "_id,abs_url"
    }
    csv.saveAsNewAPIHadoopFile(
        path='-',
        outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
        keyClass="org.apache.hadoop.io.NullWritable",
        valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
        conf=es_write_conf)
    print "-------------------- JOB COMPLETED ---------------------------"
