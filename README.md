In ogtoes.sh set:

*NUM_CORES=*
*DRIVER_MEM=1g
*BASE_PATH="$HOME/crawler"

and change **localhost** to url of ES cluster.


It looks for these files:

*$BASE_PATH/elasticsearch-hadoop-6.4.1/dist/elasticsearch-hadoop-6.4.1.jar : ES jar file for Spark
*$BASE_PATH/code/logstoES.py :  python program
*$BASE_PATH/csv_data_sample/ :  data file


run program:

```logtoes.sh fileName```


Set this enviroment variable:

```export SPARK_HOME="/home/ubuntu/spark-2.3.0-bin-hadoop2.7"```


Check to make sure all these Python packages are installed:


*locale
*logging
*json
*hashlib


