#!/bin/bash

curl -XPUT --header 'Content-Type: application/json'   http://localhost:9200/walker_pandas/_doc/1 -d '{
"hostname": "www.panda-ticket.com",
      "url": "https://www.panda-ticket.com/fr/products/102092/manu-katche"
}'

curl -XPUT --header 'Content-Type: application/json' http://localhost:9200/walker_csv/_doc/1 -d '{
                "Chiffre daffaires": 100,
                "Durée moyenne des sessions": "00:01:19",
                "Page de destination": "fr/products/102092/manu-katche",
                "Transactions": 2.0
}'

curl -XPUT --header 'Content-Type: application/json' http://localhost:9200/walker_csv/_doc/2 -d '{
                "Chiffre daffaires": 200,
                "Durée moyenne des sessions": "00:01:19",
                "Page de destination": "fr/products/102092/manu-katche",
                "Transactions": 3.0
}'

curl -XPUT --header 'Content-Type: application/json' http://localhost:9200/walker_csv/_doc/3 -d '{
                "Chiffre daffaires": 300,
                "Durée moyenne des sessions": "00:01:19",
                "Page de destination": "fr/products/102092/manu-katche",
                "Transactions": 3.0
}'